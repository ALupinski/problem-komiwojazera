﻿namespace Mapa
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PointList = new System.Windows.Forms.ListView();
            this.indexPoint = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.yColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CashDeliver = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TextBoxY = new System.Windows.Forms.TextBox();
            this.TextBoxX = new System.Windows.Forms.TextBox();
            this.YLabel = new System.Windows.Forms.Label();
            this.Xlabel = new System.Windows.Forms.Label();
            this.Wspolrzedne = new System.Windows.Forms.Label();
            this.CashText = new System.Windows.Forms.TextBox();
            this.CashL = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.SetAsStartPoint = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PointList
            // 
            this.PointList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.indexPoint,
            this.xColumn,
            this.yColumn,
            this.CashDeliver});
            this.PointList.FullRowSelect = true;
            this.PointList.GridLines = true;
            this.PointList.HideSelection = false;
            this.PointList.Location = new System.Drawing.Point(12, 12);
            this.PointList.MultiSelect = false;
            this.PointList.Name = "PointList";
            this.PointList.Size = new System.Drawing.Size(244, 466);
            this.PointList.TabIndex = 0;
            this.PointList.UseCompatibleStateImageBehavior = false;
            this.PointList.View = System.Windows.Forms.View.Details;
            this.PointList.SelectedIndexChanged += new System.EventHandler(this.PointList_SelectedIndexChanged);
            // 
            // indexPoint
            // 
            this.indexPoint.Text = "ID";
            this.indexPoint.Width = 29;
            // 
            // xColumn
            // 
            this.xColumn.Text = "X";
            // 
            // yColumn
            // 
            this.yColumn.Text = "Y";
            // 
            // CashDeliver
            // 
            this.CashDeliver.Text = "Cash";
            this.CashDeliver.Width = 88;
            // 
            // TextBoxY
            // 
            this.TextBoxY.Location = new System.Drawing.Point(286, 54);
            this.TextBoxY.Name = "TextBoxY";
            this.TextBoxY.Size = new System.Drawing.Size(121, 20);
            this.TextBoxY.TabIndex = 11;
            this.TextBoxY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxY_KeyPress);
            // 
            // TextBoxX
            // 
            this.TextBoxX.Location = new System.Drawing.Point(286, 28);
            this.TextBoxX.Name = "TextBoxX";
            this.TextBoxX.Size = new System.Drawing.Size(121, 20);
            this.TextBoxX.TabIndex = 10;
            this.TextBoxX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxX_KeyPress);
            // 
            // YLabel
            // 
            this.YLabel.AutoSize = true;
            this.YLabel.Location = new System.Drawing.Point(263, 57);
            this.YLabel.Name = "YLabel";
            this.YLabel.Size = new System.Drawing.Size(17, 13);
            this.YLabel.TabIndex = 9;
            this.YLabel.Text = "Y:";
            // 
            // Xlabel
            // 
            this.Xlabel.AutoSize = true;
            this.Xlabel.Location = new System.Drawing.Point(263, 31);
            this.Xlabel.Name = "Xlabel";
            this.Xlabel.Size = new System.Drawing.Size(17, 13);
            this.Xlabel.TabIndex = 8;
            this.Xlabel.Text = "X:";
            // 
            // Wspolrzedne
            // 
            this.Wspolrzedne.AutoSize = true;
            this.Wspolrzedne.Location = new System.Drawing.Point(262, 12);
            this.Wspolrzedne.Name = "Wspolrzedne";
            this.Wspolrzedne.Size = new System.Drawing.Size(105, 13);
            this.Wspolrzedne.TabIndex = 7;
            this.Wspolrzedne.Text = "Współrzędne Startu:";
            // 
            // CashText
            // 
            this.CashText.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CashText.Location = new System.Drawing.Point(304, 80);
            this.CashText.Name = "CashText";
            this.CashText.Size = new System.Drawing.Size(103, 21);
            this.CashText.TabIndex = 13;
            this.CashText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CashText_KeyPress);
            // 
            // CashL
            // 
            this.CashL.AutoSize = true;
            this.CashL.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CashL.Location = new System.Drawing.Point(263, 83);
            this.CashL.Name = "CashL";
            this.CashL.Size = new System.Drawing.Size(35, 14);
            this.CashL.TabIndex = 12;
            this.CashL.Text = "Cash:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(262, 446);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 32);
            this.button1.TabIndex = 14;
            this.button1.Text = "Wróć";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(263, 407);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(144, 33);
            this.Save.TabIndex = 15;
            this.Save.Text = "Zapisz Edycje";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(263, 366);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(144, 35);
            this.AddButton.TabIndex = 16;
            this.AddButton.Text = "Dodaj Punkt";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // SetAsStartPoint
            // 
            this.SetAsStartPoint.Location = new System.Drawing.Point(263, 328);
            this.SetAsStartPoint.Name = "SetAsStartPoint";
            this.SetAsStartPoint.Size = new System.Drawing.Size(144, 32);
            this.SetAsStartPoint.TabIndex = 17;
            this.SetAsStartPoint.Text = "Ustaw Jako punkt startowy";
            this.SetAsStartPoint.UseVisualStyleBackColor = true;
            this.SetAsStartPoint.Click += new System.EventHandler(this.SetAsStartPoint_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 490);
            this.Controls.Add(this.SetAsStartPoint);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CashText);
            this.Controls.Add(this.CashL);
            this.Controls.Add(this.TextBoxY);
            this.Controls.Add(this.TextBoxX);
            this.Controls.Add(this.YLabel);
            this.Controls.Add(this.Xlabel);
            this.Controls.Add(this.Wspolrzedne);
            this.Controls.Add(this.PointList);
            this.MaximumSize = new System.Drawing.Size(435, 528);
            this.MinimumSize = new System.Drawing.Size(435, 528);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView PointList;
        private System.Windows.Forms.ColumnHeader indexPoint;
        private System.Windows.Forms.ColumnHeader xColumn;
        private System.Windows.Forms.ColumnHeader yColumn;
        private System.Windows.Forms.ColumnHeader CashDeliver;
        private System.Windows.Forms.TextBox TextBoxY;
        private System.Windows.Forms.TextBox TextBoxX;
        private System.Windows.Forms.Label YLabel;
        private System.Windows.Forms.Label Xlabel;
        private System.Windows.Forms.Label Wspolrzedne;
        private System.Windows.Forms.TextBox CashText;
        private System.Windows.Forms.Label CashL;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button SetAsStartPoint;
    }
}