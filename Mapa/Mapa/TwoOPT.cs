﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapa
{
    class TwoOPT
    {
        public static bool TwoOptDoIt(List<DataRoute> helper)
        {
            foreach (DataRoute dataHelper in helper)
            {
                DataRoute bestR = new DataRoute(dataHelper);
                int bestDistance = calculateNewRoute(bestR);

                int n = dataHelper.getRouteList().Count;
              
                for (int i = 1; i < n - 2; i++)
                {
                    for (int j = i + 1; j < n - 1; j++)
                    {
                        DataRoute newRoute = new DataRoute(dataHelper);

                        newRoute.getRouteList().Reverse(i, j - i + 1);

                        int current = calculateNewRoute(newRoute);

                        if (current < bestDistance)
                        {
                            dataHelper.CopyFrom(newRoute);

                            bestDistance = current;

                            return true;
                        }
                    }
                }
                
            }

            return false;
        }

        public static int calculateNewRoute(DataRoute newRoute)
        {
            int profit = 0;
            for (int i = 0; i < newRoute.getRouteList().Count - 1; i++)
            {
                profit += DistanceMatrix.matrix[newRoute.getRouteList()[i], newRoute.getRouteList()[i + 1]].distance;
            }
            return profit;
        }
    }
}
