﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapa
{
    class ListOfRoute
    {
        public  List<DataRoute> routeList;
        public int maxLeng;
        public ListOfRoute()
        {
            maxLeng = 0;
            routeList = new List<DataRoute>();
        }
        public List<DataRoute> getList()
        {
            return routeList;
        }
        public void add(DataRoute n)
        {
            maxLeng += n.length;
            routeList.Add(n);
        }
        public int count()
        {
            return routeList.Count;
        }
        public void destroy()
        {
            maxLeng = 0;
            routeList.Clear();
        }
        public void copyFromTo( ListOfRoute to)
        {
            to.maxLeng = maxLeng;
            foreach (DataRoute s in routeList)
                to.add(s);
        }
    }
}
