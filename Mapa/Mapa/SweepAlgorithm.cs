﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Mapa
{

    class SweepAlgorithm
    {
        public static int sumaMin = 0;
        public static bool[,] boolTab = new bool[FileReader.atmNumber + 1, FileReader.atmNumber + 1];
        public static ListOfRoute c = new ListOfRoute();
        public static ListOfRoute AllRoute = new ListOfRoute();
        public static double cashLimit = FileReader.cashLimit;
        public static int lengthLimit = FileReader.lenghtLimit;
        public static List<atmData> sortedListToAlg;
        public static void write(List<atmData> a, StreamWriter sw)
        {
            foreach (atmData x in a)
                sw.WriteLine(x.x + " " + x.y);
            sw.WriteLine();

        }
        public static void writeToOne(List<atmData> from, List<atmData> to)
        {
            foreach (atmData x in from)
                to.Add(x);

        }
        public static void createListToSweep(List<atmData> atmListHelp)
        {
            List<atmData> under = FileReader.underStartPoint(atmListHelp);
            List<atmData> above = FileReader.aboveStartPoint(atmListHelp);
            List<atmData> aboveLeft = new List<atmData>();
            List<atmData> aboveRight = new List<atmData>();
            List<atmData> underLeft = new List<atmData>();
            List<atmData> underRight = new List<atmData>();
            sortedListToAlg = new List<atmData>();
            foreach (atmData i in above)
            {
                if (i.x < FileReader.startPoint.x)
                    aboveLeft.Add(i);
                else
                    aboveRight.Add(i);
            }
            foreach (atmData i in under)
            {
                if (i.x > FileReader.startPoint.x)
                    underLeft.Add(i);
                else
                    underRight.Add(i);
            }


            for (int i = 0; i < aboveLeft.Count; i++)
            {
                aboveLeft[i].cash = Math.Atan2(FileReader.startPoint.y- aboveLeft[i].y , FileReader.startPoint.x - aboveLeft[i].x) * 180 / Math.PI+Math.PI/2;
            }
          /*  aboveLeft.Sort((a, b) =>
            {
                return (int)(a.cash - b.cash);
            });*/





            for (int i = 0; i < aboveRight.Count; i++)
            {
                aboveRight[i].cash = Math.Atan2( FileReader.startPoint.y - aboveRight[i].y, FileReader.startPoint.x- aboveRight[i].x) * 180 / Math.PI;
            }
       /*     aboveRight.Sort((b, a) =>
            {
                return (int)(a.cash - b.cash);
            });*/





            for (int i = 0; i < underRight.Count; i++)
            {
                underRight[i].cash = Math.Atan2(FileReader.startPoint.y - underRight[i].y, FileReader.startPoint.x - underRight[i].x) * 180 / Math.PI + 3* Math.PI /2;
            }
     /*       underRight.Sort((a, b) =>
            {
                return (int)(a.cash - b.cash);
            });*/




            for (int i = 0; i < underLeft.Count; i++)
            {
                underLeft[i].cash = Math.Atan2(FileReader.startPoint.y - underLeft[i].y, FileReader.startPoint.x- underLeft[i].x ) * 180 / Math.PI + Math.PI;
            }
     /*       underLeft.Sort((b, a) =>
            {
                return (int)(a.cash - b.cash);
            });*/
     
            writeToOne(aboveLeft, sortedListToAlg);
            writeToOne(aboveRight, sortedListToAlg);
            writeToOne(underRight, sortedListToAlg);
            writeToOne(underLeft, sortedListToAlg);
            sortedListToAlg.Sort((a,b) =>
            {
                return (int)(a.cash - b.cash);
            });
        }
        public static void WriteList(ListOfRoute kkk)
        {
            int xTerator = 0;
            List<DataRoute> newList = new List<DataRoute>(kkk.getList());
            foreach (DataRoute helper in newList)
            {
                Console.WriteLine(xTerator++);
                foreach (int ax in helper.getRouteList())
                    Console.Write(ax + "  ");
                Console.WriteLine();
                Console.WriteLine("Długość: " + helper.getLength() + "   Kasa: " + helper.getCash());

                Console.WriteLine("Ilość ścieżek:  " + helper.getRouteList().Count());
                Console.WriteLine();
                Console.WriteLine();
                //Console.ReadKey();
            }
        }
        public static void newRoute(List<atmData> sortedList, int indexStartInTab)
        {
            List<atmData> helper = new List<atmData>(sortedList);

            DataRoute newR = new DataRoute();
            for (int i = indexStartInTab; i < FileReader.atmNumber; i++)
            {
                double x = newR.getCash();
                
                if (x + FileReader.atmList[helper[indexStartInTab].index-1].cash <= cashLimit)
                {
                    int l = newR.getLength();
                    if (l + DistanceMatrix.matrix[newR.getLastIndex(), helper[indexStartInTab].index].distance <= lengthLimit - DistanceMatrix.maxLen)
                    {
                        newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), helper[indexStartInTab].index].distance, FileReader.atmList[helper[indexStartInTab].index - 1]);
                        helper.RemoveAt(indexStartInTab);
                    }
                    else if (l + DistanceMatrix.matrix[newR.getLastIndex(), helper[indexStartInTab].index].distance > lengthLimit - DistanceMatrix.maxLen)
                    {
                        newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), FileReader.startPoint.index].distance, FileReader.startPoint);
                        c.add(newR);
                                      
                        newR = new DataRoute();
                        i--;

                    }
                }
                else if (x + helper[indexStartInTab].cash > cashLimit)
                {
                    newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), FileReader.startPoint.index].distance, FileReader.startPoint);
                    c.add(newR);
                    newR = new DataRoute();
                    i--;

                }
                if (indexStartInTab == 0 && i == FileReader.atmNumber - 1)
                {
                    newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), FileReader.startPoint.index].distance, FileReader.startPoint);
                    c.add(newR);

                }

            }
            if (indexStartInTab != 0)
            {
                for (int i = 0; i < indexStartInTab; i++)
                {
                    double x = newR.getCash();
                    if (x + helper[0].cash <= cashLimit)
                    {
                        int l = newR.getLength();
                        if (l + DistanceMatrix.matrix[newR.getLastIndex(), helper[0].index].distance <= lengthLimit - DistanceMatrix.maxLen)
                        {
                            newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), helper[0].index].distance, FileReader.atmList[helper[0].index - 1]);
                            helper.RemoveAt(0);

                        }
                        else if (l + DistanceMatrix.matrix[newR.getLastIndex(), helper[0].index].distance > lengthLimit - DistanceMatrix.maxLen)
                        {
                            newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), FileReader.startPoint.index].distance, FileReader.startPoint);
                            c.add(newR);
                            newR = new DataRoute();
                            i--;

                        }
                    }
                    else if (x + helper[0].cash > cashLimit)
                    {
                        newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), FileReader.startPoint.index].distance, FileReader.startPoint);
                        c.add(newR);
                        newR = new DataRoute();
                        i--;


                    }
                    if (i == indexStartInTab - 1)
                    {
                        newR.addData(DistanceMatrix.matrix[newR.getLastIndex(), FileReader.startPoint.index].distance, FileReader.startPoint);
                        c.add(newR);

                    }
                }
            }

        }
        public static int lengggth = 0, index = 0;
        public static void saveToFile(ListOfRoute sortedList)
        {
            StreamWriter newRead = new StreamWriter("trasy.txt");
            List<DataRoute> newList = new List<DataRoute>(sortedList.getList());
            foreach (DataRoute helper in newList)
            {
                foreach (int ax in helper.getRouteList())
                {
                    if (ax == 0)
                        newRead.Write(FileReader.atmNumber + 1 + " ");
                    else
                       newRead.Write(ax  + " ");
                }
                //newRead.WriteLine("Długość: " + helper.getLength() + "   Kasa: " + helper.getCash());
                newRead.WriteLine();
                //newRead.WriteLine("Ilość Punktów:  " + helper.getRouteList().Count());
               
            }
            newRead.Close();
        }
        public static void insert()
        {

            foreach (DataRoute a in c.getList().ToList())
            {
                List<atmData> newList = new List<atmData>();
                atmData helper = new atmData();
                List<int> helperList = new List<int>(a.getRouteList());
                int possibleLength = FileReader.lenghtLimit - a.getLength();
                double possibleCash = FileReader.cashLimit - a.getCash();

                if (possibleCash > 0 && possibleLength > 0)
                {
                    for (int i = 0; i < helperList.Count - 2; i++)
                    {
                        List<atmData> atmOfList = new List<atmData>(FileReader.atmList);

                        for (int s = 0; s < atmOfList.Count; s++)
                        {
                            int leng = DistanceMatrix.matrix[helperList[i], atmOfList[s].index].distance + DistanceMatrix.matrix[helperList[i + 1], atmOfList[s].index].distance;
                            if (leng <= possibleLength)
                            {

                                if (atmOfList[s].cash <= possibleCash)
                                {
                                    if (atmOfList[s].cash > helper.cash)
                                    {
                                        lengggth = leng;
                                        helper = new atmData(atmOfList[s]);
                                        index = s;
                                    }
                                }
                            }
                        }
                    }


                }


                DataRoute newData = new DataRoute(lengggth, a.getCash() + helper.cash, a.getRouteList(), a.getLastInListPoint());
                AllRoute.add(newData);

            }
        }
        public static void check(ListOfRoute c)
        {
            List<DataRoute> routeList = c.getList();
            if (AllRoute.routeList.Count == 0)
            {
                foreach (DataRoute x in c.getList())
                {
                    AllRoute.add(x);
                }
                AllRoute.maxLeng = c.maxLeng;
                sumaMin = c.maxLeng;
            }
            else
            {
                if(c.maxLeng<sumaMin)
                {
                    AllRoute.destroy();
                    foreach (DataRoute x in c.getList())
                    {
                        AllRoute.add(x);
                    }
                    AllRoute.maxLeng = c.maxLeng;
                    sumaMin = c.maxLeng;
                }
            }
        }
        public static void initTab()
        {
            for (int k = 0; k < FileReader.atmNumber + 1; k++)
            {
                for (int l = 0; l < FileReader.atmNumber + 1; l++)
                {
                    boolTab[k, l] = false;
                }
            }
        }
        public static void deleteDuplicate(ListOfRoute a)
        {
            AllRoute.destroy(); 
            initTab();
            int i = 0;
            foreach ( DataRoute data in a.routeList)
            {
                int p, k;
                p = data.route[1];
                k = data.route[data.route.Count - 2];
                if (boolTab[p, k] == false)
                {
                    boolTab[p, k] = true;
                    AllRoute.add(data);
                }
             

            }
        }
        public static void calc(ListOfRoute newListOfRoute)
        {
            List<DataRoute> dataRouteList = newListOfRoute.routeList;
            int newSum = 0;
            foreach(DataRoute d in dataRouteList)
            {
                d.calculate();
                newSum += d.length;
            }
            newListOfRoute.maxLeng = newSum;
        }
        public static int initMerge(ListOfRoute listRouteHelper)
        {

            int isOneMerge=0;
            List<string> newStringList = new List<string>();
            AllRoute.destroy();
            DataRoute newRoute = new DataRoute();
            for (int i = 0; i < listRouteHelper.routeList.Count - 1; i++)
            {
                int ind = listRouteHelper.routeList.Count;
                if (listRouteHelper.routeList.Count % 2 == 1)
                {
                    newRoute.CopyFrom(listRouteHelper.routeList[ind - 1]);
                    listRouteHelper.routeList.RemoveAt(ind - 1);
                   /* if (listRouteHelper.routeList[ind - 1].deliveryCash + listRouteHelper.routeList[ind - 2].deliveryCash < FileReader.cashLimit)
                    {
                        if (listRouteHelper.routeList[ind - 1].length + listRouteHelper.routeList[ind - 2].length < FileReader.lenghtLimit)
                        {

                            listRouteHelper.routeList[ind - 2].route.RemoveAt(listRouteHelper.routeList[ind - 2].route.Count - 1);
                            for (int k = 1; k < listRouteHelper.routeList[ind - 1].route.Count - 1; k++)
                            {
                                listRouteHelper.routeList[ind - 2].route.Add(listRouteHelper.routeList[ind - 1].route[k]);
                            }
                            listRouteHelper.routeList.RemoveAt(ind - 1);
                        }
                    }*/
                }
                
                if (listRouteHelper.routeList[i].deliveryCash + listRouteHelper.routeList[i + 1].deliveryCash < FileReader.cashLimit)
                {
                    if (listRouteHelper.routeList[i].length + listRouteHelper.routeList[i + 1].length < FileReader.lenghtLimit)
                    {
                        isOneMerge = 1;
                        int j = i + 1;
                        newStringList.Add(i + " " + j);
                        i++;

                    }
                    else
                    {
                        AllRoute.add(listRouteHelper.routeList[i]);
                        AllRoute.add(listRouteHelper.routeList[i + 1]);
                    }
                }
                else
                {
                    AllRoute.add(listRouteHelper.routeList[i]);
                    AllRoute.add(listRouteHelper.routeList[i + 1]);
                }


            }
            foreach(string str in newStringList)
            {
                string[] s = str.Split(' ');
                int i = int.Parse(s[0]);
                int j = int.Parse(s[1]);
                merge(listRouteHelper.routeList[i], listRouteHelper.routeList[j]);
            }
            AllRoute.add(newRoute);
            return isOneMerge;
        }
        public static void merge(DataRoute first, DataRoute second)
        {
            first.route.RemoveAt(first.route.Count - 1);
            first.deliveryCash += second.deliveryCash;


            //second.route.RemoveAt(0);
            //foreach (int help in second.route)
            for (int i = 1; i < second.route.Count; i++)
            {
                first.route.Add(second.route[i]);
            }
            
            first.calculate();

            AllRoute.add(first);

        }
        public static string sweepAlgoithm()
        {
            initTab();
            int iter = 0;
            foreach (atmData startPoint in sortedListToAlg.ToList())
            {
                newRoute(sortedListToAlg, iter);
                check(c);
                iter++;
                c.destroy();
            }
            
            List<DataRoute> helpList = new List<DataRoute>(AllRoute.getList());

   
            
            bool change;

            do
            {
                change = false;

                change |= TwoOPT.TwoOptDoIt(helpList);

            } while (change);
            calc(AllRoute);
            int x = 0;
            
            do
            {
                ListOfRoute newList = new ListOfRoute();
                AllRoute.copyFromTo(newList);
                x = initMerge(newList);
                helpList = new List<DataRoute>(AllRoute.getList());
                do
                {
                    change = false;
                    change |= TwoOPT.TwoOptDoIt(helpList);
                } while (change);
                calc(AllRoute);
            } while (x != 0);
            ListOfRoute newLista = new ListOfRoute();
            AllRoute.copyFromTo(newLista);
            deleteDuplicate(newLista);
            saveToFile(AllRoute);
            return "SUma dróg :       " + AllRoute.maxLeng ;
          
        }
        
    }
}
