﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mapa
{
    class Swap
    {
        public static int count = 0;
        public static void SwapDoIt(List<DataRoute> helper)
        {
            SweepAlgorithm.AllRoute.destroy();
            foreach (DataRoute dataHelper in helper)
            {
                DataRoute newRoute = new DataRoute(dataHelper);
                for (int i = 1; i < dataHelper.getRouteList().Count - 2; i++)
                {
                    for (int j = i + 1; j < dataHelper.getRouteList().Count - 1; j++)
                    {
                        int oldD = DistanceMatrix.matrix[i - 1, i].distance + DistanceMatrix.matrix[i, i + 1].distance + DistanceMatrix.matrix[j - 1, j].distance + DistanceMatrix.matrix[j, j + 1].distance;
                        int newD = DistanceMatrix.matrix[i - 1, j].distance + DistanceMatrix.matrix[j, i + 1].distance + DistanceMatrix.matrix[j - 1, i].distance + DistanceMatrix.matrix[i, j + 1].distance;
                        if (oldD > newD)
                        {
                            int temp = newRoute.getRouteList()[i];
                            newRoute.getRouteList()[i] = newRoute.getRouteList()[j];
                            newRoute.getRouteList()[j] = temp;
                        }
                    }
                }
                SweepAlgorithm.AllRoute.add(newRoute);
            }
        }
        public static void Swap2(List<DataRoute> helper)
        {
            foreach(DataRoute DataHelp in helper)
            {
                DataRoute newDataRoute = new DataRoute(DataHelp);
                bool inf = true;
                while (inf != false)
                {
                    bool s = false;
                    for (int i = 1; i < newDataRoute.getRouteList().Count - 2; i++)
                    {
                        for (int j = i + 1; j < newDataRoute.getRouteList().Count - 1; j++)
                        {
                            
                            int temp = newDataRoute.getRouteList()[i];
                            newDataRoute.getRouteList()[i] = newDataRoute.getRouteList()[j];
                            newDataRoute.getRouteList()[j] = temp;
                            int profitHelp = calculateNewRoute(newDataRoute);
                            if (profitHelp < newDataRoute.length)
                            {
                                newDataRoute.length = profitHelp;
                                newDataRoute.route = new List<int>(newDataRoute.route);
                                s = true;
                            }
                        }
                    }
                    if(s==false)
                    {
                        inf = true;
                    }
                }
                SweepAlgorithm.AllRoute.add(newDataRoute);
            }
        }
        public static int calculateNewRoute(DataRoute newRoute)
        {
            int profit = 0;
            for (int i = 0; i < newRoute.getRouteList().Count-1; i++)
            {
                profit += DistanceMatrix.matrix[newRoute.getRouteList()[i], newRoute.getRouteList()[i + 1]].distance;
            }
            return profit;
        }
       /* public static void saveToFile(List<DataRoute> newRoute)
        {
            List<int> helpList = new List<int>(newRoute.getRouteList());
            string name = helpList[1].ToString() + "-" + helpList[helpList.Count - 2].ToString();
            StreamWriter newRead = new StreamWriter("solution.txt");
            foreach (int ax in helpList)
                newRead.Write(ax + " ");
            newRead.WriteLine("Długość: " + newRoute.getLength() + "   Kasa: " + newRoute.getCash());
            newRead.WriteLine("Ilość Punktów:  " + helpList.Count());
            newRead.Close();
        }*/
    }
}
