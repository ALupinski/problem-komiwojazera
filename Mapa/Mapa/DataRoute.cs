﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapa
{
    class DataRoute
    {
        public int length;
        public double deliveryCash;
        public List<int> route;
        public atmData lastInList;
        public void setLast(atmData point)
        {
            lastInList = point;
        }

        public void CopyFrom(DataRoute another)
        {
            length = another.length;
            deliveryCash = another.deliveryCash;
            route = new List<int>(another.route);
            lastInList = another.lastInList;
        }

        public DataRoute()
        {
            length = 0;
            deliveryCash = 0;
            route = new List<int>();
            route.Add(FileReader.startPoint.index);
            lastInList = FileReader.startPoint;
        }

        public DataRoute(DataRoute newData)
        {
            length = newData.length;
            deliveryCash = newData.deliveryCash;
            route = new List<int>(newData.route);
            lastInList = newData.lastInList;
        }

        public DataRoute(int length, double deliveryCash, List<int> route, atmData lastInList)
        {
            this.length = length;
            this.deliveryCash = deliveryCash;
            this.route = route;
            this.lastInList = lastInList;
        }

        public void InsertAt(int index, int a)
        {
            route.Insert(index, a);
        }

        public List<int> getRouteList()
        {
            return route;
        }

        public void setLength(int l)
        {
            length += l;
        }

        public void setCash(double l)
        {
            deliveryCash += l;
        }

        public void addData(int distance, atmData point)
        {
            length = length + distance;
            deliveryCash += point.cash;
            route.Add(point.index);
            lastInList = point;
        }



        public int getLength()
        {
            return length;
        }

        public double getCash()
        {
            return deliveryCash;
        }

        public int getLastIndex()
        {
            return lastInList.index;
        }

        public int getIndexBeforeLast()
        {
            return route[route.Count - 2];
        }

        public atmData getLastInListPoint()
        {
            return lastInList;
        }
        public void calculate()
        {
            int x = 0;

            for (int i = 0; i < route.Count - 1; i++)
            {
                x += DistanceMatrix.matrix[route[i], route[i + 1]].distance;
            }

            length = x;
        }
    }
}
