﻿namespace Mapa
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.WarsawData = new System.Windows.Forms.Button();
            this.Testowe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wybierz zestaw danych:";
            // 
            // WarsawData
            // 
            this.WarsawData.Location = new System.Drawing.Point(133, 64);
            this.WarsawData.Name = "WarsawData";
            this.WarsawData.Size = new System.Drawing.Size(89, 36);
            this.WarsawData.TabIndex = 1;
            this.WarsawData.Text = "Warsaw Data";
            this.WarsawData.UseVisualStyleBackColor = true;
            this.WarsawData.Click += new System.EventHandler(this.WarsawData_Click);
            // 
            // Testowe
            // 
            this.Testowe.Location = new System.Drawing.Point(12, 64);
            this.Testowe.Name = "Testowe";
            this.Testowe.Size = new System.Drawing.Size(89, 36);
            this.Testowe.TabIndex = 2;
            this.Testowe.Text = "Test Data";
            this.Testowe.UseVisualStyleBackColor = true;
            this.Testowe.Click += new System.EventHandler(this.Testowe_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 112);
            this.Controls.Add(this.Testowe);
            this.Controls.Add(this.WarsawData);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(250, 150);
            this.MinimumSize = new System.Drawing.Size(250, 150);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button WarsawData;
        private System.Windows.Forms.Button Testowe;
    }
}