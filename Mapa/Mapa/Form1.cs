﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.MapProviders;
namespace Mapa
{
    public partial class Form1 : Form
    {
        public GMap.NET.WindowsForms.GMapOverlay markers;
        public GMap.NET.WindowsForms.GMapOverlay routes;
        static DataRoute[] solution;
        public static int ilKur=0;
        public  static int startV;
        public Form1(int startUpValue)
        {
            InitializeComponent();

            this.WindowState = FormWindowState.Maximized;
            if (startUpValue == 1)
            {
                startV = 1;
                FileReader.GetListFromFile();
                markers = new GMap.NET.WindowsForms.GMapOverlay("markers");
                routes = new GMap.NET.WindowsForms.GMapOverlay("routes");
                gMap.Position = new GMap.NET.PointLatLng(FileReader.startPoint.x, FileReader.startPoint.y);
                gMap.SetPositionByKeywords("center");
                gMap.Zoom = 0;
                TextBoxX.Text = FileReader.startPoint.x.ToString();
                TextBoxY.Text = FileReader.startPoint.y.ToString();
                MaxLen.Text = FileReader.lenghtLimit.ToString();
                CashToDel.Text = FileReader.cashLimit.ToString();
                setMarker();
                DistanceMatrix.createDistanceMatrix(FileReader.atmList);



            }
            else if (startUpValue == 0)
            {
                startV = 0;
                markers = new GMap.NET.WindowsForms.GMapOverlay("markers");
                routes = new GMap.NET.WindowsForms.GMapOverlay("routes");
                gMap.Position = new GMap.NET.PointLatLng(52.2297700, 21.0117800);
                gMap.SetPositionByKeywords("Warsaw, Poland");
                getWarsFromFile();
                getPointers();
                
            }
        }
        public void setMarker()
        {
            
            foreach (atmData a in FileReader.atmList)
            {
                double s = a.x / 12;
                double w = a.y / 6;
                GMapMarker marker =
                        new GMarkerGoogle(new PointLatLng(s, w), GetCol(a.cash));
                marker.ToolTipText = "x: " + s.ToString() + " y: " + w.ToString();
                markers.Markers.Add(marker);
            }
            gMap.Overlays.Add(markers);
        }

        public void getWarsFromFile()
        {
            StreamReader file = new StreamReader("Wejście.txt");
            string reader = file.ReadLine();
            int lineToRead = int.Parse(reader);
            FileReader.atmNumber = lineToRead;
            for (int i = 0; i < lineToRead; i++)
            {
                reader = file.ReadLine();
                string[] nowy = reader.Split('"');
                double s = double.Parse(nowy[0].Replace('.', ','));
                double w = double.Parse(nowy[1].Replace('.', ','));
                double c = double.Parse(nowy[2].Replace('.', ','));
                atmData newAdder = new atmData();
                newAdder.index = FileReader.atmList.Count + 1;
                newAdder.x = s;
                newAdder.y = w;
                newAdder.cash = c;
                FileReader.atmList.Add(newAdder);
            }
        }
        public GMarkerGoogleType GetCol(double cash)
        {
            if (cash < 0 && cash <= 200)
                return GMarkerGoogleType.yellow_pushpin;
            else if (cash > 200 && cash <= 400)
                return GMarkerGoogleType.red_pushpin;
            else if (cash > 400 && cash <= 600)
                return GMarkerGoogleType.green_pushpin;
            else if (cash > 600 && cash < 1000)
                return GMarkerGoogleType.blue_pushpin;
            else
             return GMarkerGoogleType.purple_pushpin;

        }
        public void getPointers()
        {
            foreach (atmData a in FileReader.atmList)
            {
                double s = a.x ;
                double w = a.y ;
                GMapMarker marker =
                        new GMarkerGoogle(new PointLatLng(s, w), GetCol(a.cash));
                marker.ToolTipText = "x: "+ s.ToString() + " y: " +w.ToString() ;
                markers.Markers.Add(marker);
            }
            gMap.Overlays.Add(markers);       
        }

        public void gMapControl1_Load(object sender, EventArgs e)
        {
            
            gMap.MapProvider = BingMapProvider.Instance;
            GMaps.Instance.Mode = AccessMode.ServerOnly;   
            gMap.ShowCenter = false;


        }

        private void loadAllRoute()
        {
            int ind = 1;
            foreach (DataRoute route in SweepAlgorithm.AllRoute.getList())
            {                
                ListViewItem newItem = new ListViewItem(ind.ToString());
                newItem.SubItems.Add(route.getLength().ToString());
                newItem.SubItems.Add(route.getCash().ToString());
                RouteList.Items.Add(newItem);
                ind++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TextBoxX.Text.ToString() == "" || TextBoxY.Text.ToString() == "" || CashToDel.ToString() == "" || MaxLen.ToString() == "")
            {
                MessageBox.Show("Brak potrzebnych danych");
                return;
            }
            else
            {
                RouteList.Items.Clear();
                
                if (startV == 1)//test
                {
                    SweepAlgorithm.createListToSweep(FileReader.atmList);
                    FileReader.startPoint.x = double.Parse(TextBoxX.Text.Replace('.', ','))/12;
                    FileReader.startPoint.y = double.Parse(TextBoxY.Text.Replace('.', ','))/6;
                    FileReader.lenghtLimit = int.Parse(MaxLen.Text);
                    FileReader.cashLimit = double.Parse(CashToDel.Text.Replace('.', ','));
                    solution = new DataRoute[ilKur];
                    string mess = SweepAlgorithm.sweepAlgoithm();
                    MessageBox.Show(mess);
                    loadAllRoute();
                }
                else
                {

                   
                    FileReader.startPoint.x = double.Parse(TextBoxX.Text.Replace('.', ','));
                    FileReader.startPoint.y = double.Parse(TextBoxY.Text.Replace('.', ','));
                    FileReader.lenghtLimit = int.Parse(MaxLen.Text);
                    FileReader.cashLimit = double.Parse(CashToDel.Text.Replace('.', ','));
                    FileReader.atmList.RemoveAt(FileReader.startPoint.index);
                    int i = 1;
                    foreach(atmData a in FileReader.atmList)
                    {
                        //int stary = a.index;
                        a.index = i;
                        i++;
                        //MessageBox.Show("stary "+ stary.ToString()  + "nowy   "+ a.index.ToString());
                    }
                    FileReader.atmNumber--;
                    DistanceMatrix.createDistanceMatrix(FileReader.atmList);
                    SweepAlgorithm.createListToSweep(FileReader.atmList);

                    string mess = SweepAlgorithm.sweepAlgoithm();
                    MessageBox.Show(mess);
                    loadAllRoute();
                }
            }
        }

        private void ListOfPoints_Click(object sender, EventArgs e)
        {
            Form3 new3 = new Form3();
            new3.ShowDialog();
            if (startV == 1)
            {
                markers.Clear();
                setMarker();
                
            }
            else if (startV == 0)
            {
                markers.Clear();
                getPointers();
                if(FileReader.startPoint.x!=0 && FileReader.startPoint.y != 0)
                {
                    TextBoxX.Text = FileReader.startPoint.x.ToString();
                    TextBoxY.Text = FileReader.startPoint.y.ToString();
                }
            }

        }

        private void TextBoxX_KeyPress(object sender, KeyPressEventArgs e)
        {

            char ch = e.KeyChar;
            if (ch == 46 && TextBoxX.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void TextBoxY_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && TextBoxY.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void CashToDel_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && CashToDel.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void RouteList_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            int ind;
            if (RouteList.SelectedItems.Count <= 0) return;
            else
            {
                int x=0;
                string n = " " ;
                ind = RouteList.SelectedItems[0].Index;
                List<int> routeToDraw = SweepAlgorithm.AllRoute.getList()[ind].getRouteList();
                markers.Clear();
                routes.Clear();
                foreach (int point in routeToDraw)
                {
                    x++;
                    n += point.ToString() + "  ";
                    double s, w;
                    if (startV == 1)
                    {
                        if (point == 0)
                        {
                            s = FileReader.startPoint.x ;
                            w = FileReader.startPoint.y ;
                            GMapMarker marker =
                                new GMarkerGoogle(new PointLatLng(s, w), GMarkerGoogleType.blue_pushpin);
                            marker.ToolTipText = s.ToString();
                            markers.Markers.Add(marker);
                     
                        }
                        else
                        {
                           // MessageBox.Show(point + "    ind otrz " + FileReader.atmList[point].index);
                            s = FileReader.atmList[point-1].x / 12;
                            w = FileReader.atmList[point-1].y / 6;
                            GMapMarker marker =
                                new GMarkerGoogle(new PointLatLng(s, w), GMarkerGoogleType.blue_pushpin);
                            marker.ToolTipText = s.ToString();
                            markers.Markers.Add(marker);
                 
                        }
                    }
                    else if (startV == 0)
                    {
                        s = FileReader.atmList[point].x;
                        w = FileReader.atmList[point].y;
                         GMapMarker marker =
                             new GMarkerGoogle(new PointLatLng(s, w), GMarkerGoogleType.blue_pushpin);
                         marker.ToolTipText = w.ToString();
                         markers.Markers.Add(marker);
     
                    }


                }
                MessageBox.Show(n + " ilość pkt "+ x);
                //gMap.Overlays.Add(markers);
                DrawRoute(routeToDraw);
               
                
            }
            

        }

        private void DrawRoute(List<int> helper)
        {
            //GMapOverlay routes = new GMapOverlay("routes");
            List<PointLatLng> rPts = new List<PointLatLng>();
            //int first = 0;
            foreach (int p in helper)
            {
               if (p == 0 )
                {
                    rPts.Add(new PointLatLng(FileReader.startPoint.x , FileReader.startPoint.y ));
                    //MessageBox.Show(FileReader.atmList[p].x/12  + " " + FileReader.atmList[p].y / 6);
                   
                }
                else
                {
                    rPts.Add(new PointLatLng(FileReader.atmList[p - 1].x / 12, FileReader.atmList[p - 1].y / 6));
                   // MessageBox.Show(FileReader.atmList[p - 1].x / 12 + " " + FileReader.atmList[p - 1].y / 6);
                }
            }
            GMapRoute route = new GMapRoute(rPts, "Route "); 
            Color[] colors = new Color[] { Color.Blue, Color.Pink, Color.Green, Color.Red, Color.Gray, Color.Yellow };
            route.Stroke = new Pen(colors[3],3);
            routes.Routes.Add(route);
            gMap.Overlays.Add(routes);


        }

        private void Close_Click(object sender, EventArgs e)
        {




            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void RouteList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int ind;
            if (RouteList.SelectedItems.Count <= 0) return;
            else
            {
                int x = 0;
                string n = " ";
                ind = RouteList.SelectedItems[0].Index;
                List<int> routeToDraw = SweepAlgorithm.AllRoute.getList()[ind].getRouteList();

                foreach (int point in routeToDraw)
                {

                    n += point.ToString() + "  ";
                }
                MessageBox.Show(" Wierzchołki w trasie \n " + n);

            }
        }
    }
}
