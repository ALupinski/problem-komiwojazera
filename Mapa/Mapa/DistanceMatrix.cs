﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapa
{
    class DistanceMatrix
    {
        public struct distanceData
        {
            public int distance;
            public double destination;
        }
        public static List<distanceData>[] distanceMatrix;
        public static distanceData[,] matrix;
        public static atmData[] helperTab;
        public static int maxLen = 0;
        public static void helperMatrix(List<atmData> pointList, int index)
        {
            int iterator = 0;

            helperTab = new atmData[index];
            helperTab[0] = FileReader.startPoint;
            iterator++;
            foreach (atmData a in pointList)
            {
                helperTab[iterator] = a;
                iterator++;

            }
        }

        public static void writeTab(int indexNumber)
        {
            for (int i = 0; i < indexNumber; i++)
            {
                foreach (distanceData a in distanceMatrix[i])
                {
                    Console.Write(a.distance + "   ");
                }
                Console.WriteLine();
                Console.WriteLine(distanceMatrix[i].Count);
            }

        }
        public static void writeMatrix(int indexNumber)
        {
            for (int i = 0; i < indexNumber; i++)
            {
                for (int j = 0; j < indexNumber; j++)
                {
                    Console.Write(matrix[i, j].distance + " ");
                }
                Console.WriteLine();
            }
        }
        public static void createDistanceMatrix(List<atmData> pointList)
        {
            int indexNumber = FileReader.atmNumber + 1;
            helperMatrix(pointList, indexNumber);
            distanceMatrix = new List<distanceData>[indexNumber];
            matrix = new distanceData[indexNumber, indexNumber];
            for (int i = 0; i < indexNumber; i++)
                distanceMatrix[i] = new List<distanceData>();
            int control = 0;
            for (int i = 0; i < indexNumber; i++)
            {

                // Console.Write(i + " ");
                for (int j = i; j < indexNumber; j++)
                {
                    if (j == i)
                    {
                        distanceData nullDistance = new distanceData();
                        nullDistance.destination = 0;
                        nullDistance.distance = int.MaxValue;
                        matrix[i, j] = nullDistance;
                        control++;
                    }
                    else
                    {
                        int xlen;
                        
                        
                        if(Form1.startV == 0)
                        {
                            double xLength, yLength,xlend;
                            xLength = helperTab[i].x - helperTab[j].x;
                            if (xLength < 0)
                                xLength = xLength * -1;
                            yLength = helperTab[i].y - helperTab[j].y;
                            if (yLength < 0)
                                yLength = yLength * -1;
                            xlend = Math.Round(Math.Sqrt(Math.Pow(xLength, 2) + Math.Pow(yLength, 2)));
                            xlen = (int)(xlend * 111111);
                        }
                        else
                        {

                            double xLength, yLength;
                            xLength = helperTab[i].x - helperTab[j].x;
                            if (xLength < 0)
                                xLength = xLength * -1;
                            yLength = helperTab[i].y - helperTab[j].y;
                            if (yLength < 0)
                                yLength = yLength * -1;

                            xlen = (int) Math.Round(Math.Sqrt(Math.Pow(xLength, 2) + Math.Pow(yLength, 2)));
                        }
                        int distance = xlen;
                        if (distance > maxLen)
                            maxLen = distance;
                        control = control + 2;
                        distanceData newDistance = new distanceData();
                        newDistance.destination = j;
                        newDistance.distance = distance;
                        matrix[i, j] = newDistance;
                        distanceMatrix[i].Add(newDistance);
                        newDistance.destination = i;     
                        matrix[j, i] = newDistance;
                    }
                }
                distanceMatrix[i].Sort((b, a) =>
                {
                    return (int)(b.distance - a.distance);
                });
            }
            // writeMatrix(indexNumber);

        }
    }
}
