﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mapa
{
    class _2OPT
    {
        public static int count = 0;
        public static void _2OptDoIt(List<DataRoute> helper, DataRoute[] solution)
        {
            double [] tab = new double[Form1.ilKur];
            SweepAlgorithm.initTab();
            foreach (DataRoute dataHelper in helper)
            {
                for (int i = 1; i < dataHelper.getRouteList().Count - 2; i++)
                {
                    DataRoute min = new DataRoute(dataHelper);
                    for (int j = i + 1; j < dataHelper.getRouteList().Count - 1; j++)
                    {
                        DataRoute newRoute = new DataRoute(dataHelper);
                        int temp = newRoute.getRouteList()[i];
                        newRoute.getRouteList()[i] = newRoute.getRouteList()[j];
                        newRoute.getRouteList()[j] = temp;
                        int profitHelp = calculateNewRoute(newRoute);
                        if (profitHelp < newRoute.length)
                        {
                            min.length = profitHelp;
                            min.route = new List<int>(newRoute.route);

                            dataHelper.route = new List<int>(min.route);
                        }
                    }
                    /* if(count ==0)
                     {
                         tab[count] = min.deliveryCash;
                         solution[count] = min;
                         SweepAlgorithm.boolTab[min.route[1], min.route[min.route.Count - 2]] = true;
                         count++;

                     }
                     else if(SweepAlgorithm.boolTab[min.route[1],min.route[min.route.Count-2]]==false)
                     {

                     }*/
                }
            }
        }
        public static int calculateNewRoute(DataRoute newRoute)
        {
            int profit = 0;
            for (int i = 0; i < newRoute.getRouteList().Count-1; i++)
            {
                profit += DistanceMatrix.matrix[newRoute.getRouteList()[i], newRoute.getRouteList()[i + 1]].distance;
            }
            return profit;
        }
      /*  public static void saveToFile(DataRoute newRoute)
        {
            List<int> helpList = new List<int>(newRoute.getRouteList());
            string name = helpList[1].ToString() + "-" + helpList[helpList.Count - 2].ToString();
            StreamWriter newRead = new StreamWriter(name + ".txt");
            foreach (int ax in helpList)
                newRead.Write(ax + " ");
            newRead.WriteLine("Długość: " + newRoute.getLength() + "   Kasa: " + newRoute.getCash());
            newRead.WriteLine("Ilość Punktów:  " + helpList.Count());
            newRead.Close();
        }*/
    }
}
