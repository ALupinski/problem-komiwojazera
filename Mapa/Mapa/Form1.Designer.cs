﻿namespace Mapa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gMap = new GMap.NET.WindowsForms.GMapControl();
            this.Wspolrzedne = new System.Windows.Forms.Label();
            this.Xlabel = new System.Windows.Forms.Label();
            this.YLabel = new System.Windows.Forms.Label();
            this.TextBoxX = new System.Windows.Forms.TextBox();
            this.TextBoxY = new System.Windows.Forms.TextBox();
            this.LabelKasa = new System.Windows.Forms.Label();
            this.CashToDel = new System.Windows.Forms.TextBox();
            this.MaxLen = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Slove = new System.Windows.Forms.Button();
            this.RouteList = new System.Windows.Forms.ListView();
            this.Number = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.len = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cashh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ListOfPoints = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // gMap
            // 
            this.gMap.AutoScroll = true;
            this.gMap.AutoSize = true;
            this.gMap.Bearing = 0F;
            this.gMap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gMap.CanDragMap = true;
            this.gMap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gMap.GrayScaleMode = false;
            this.gMap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMap.LevelsKeepInMemmory = 5;
            this.gMap.Location = new System.Drawing.Point(247, 12);
            this.gMap.MarkersEnabled = true;
            this.gMap.MaximumSize = new System.Drawing.Size(1159, 796);
            this.gMap.MaxZoom = 18;
            this.gMap.MinimumSize = new System.Drawing.Size(1159, 796);
            this.gMap.MinZoom = 2;
            this.gMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gMap.Name = "gMap";
            this.gMap.NegativeMode = false;
            this.gMap.PolygonsEnabled = true;
            this.gMap.RetryLoadTile = 0;
            this.gMap.RoutesEnabled = true;
            this.gMap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMap.ShowTileGridLines = false;
            this.gMap.Size = new System.Drawing.Size(1159, 796);
            this.gMap.TabIndex = 0;
            this.gMap.Zoom = 13D;
            this.gMap.Load += new System.EventHandler(this.gMapControl1_Load);
            // 
            // Wspolrzedne
            // 
            this.Wspolrzedne.AutoSize = true;
            this.Wspolrzedne.Location = new System.Drawing.Point(1412, 12);
            this.Wspolrzedne.Name = "Wspolrzedne";
            this.Wspolrzedne.Size = new System.Drawing.Size(105, 13);
            this.Wspolrzedne.TabIndex = 1;
            this.Wspolrzedne.Text = "Współrzędne Startu:";
            // 
            // Xlabel
            // 
            this.Xlabel.AutoSize = true;
            this.Xlabel.Location = new System.Drawing.Point(1413, 31);
            this.Xlabel.Name = "Xlabel";
            this.Xlabel.Size = new System.Drawing.Size(17, 13);
            this.Xlabel.TabIndex = 3;
            this.Xlabel.Text = "X:";
            // 
            // YLabel
            // 
            this.YLabel.AutoSize = true;
            this.YLabel.Location = new System.Drawing.Point(1413, 57);
            this.YLabel.Name = "YLabel";
            this.YLabel.Size = new System.Drawing.Size(17, 13);
            this.YLabel.TabIndex = 4;
            this.YLabel.Text = "Y:";
            // 
            // TextBoxX
            // 
            this.TextBoxX.Location = new System.Drawing.Point(1436, 28);
            this.TextBoxX.Name = "TextBoxX";
            this.TextBoxX.Size = new System.Drawing.Size(140, 20);
            this.TextBoxX.TabIndex = 5;
            this.TextBoxX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxX_KeyPress);
            // 
            // TextBoxY
            // 
            this.TextBoxY.Location = new System.Drawing.Point(1436, 54);
            this.TextBoxY.Name = "TextBoxY";
            this.TextBoxY.Size = new System.Drawing.Size(140, 20);
            this.TextBoxY.TabIndex = 6;
            this.TextBoxY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxY_KeyPress);
            // 
            // LabelKasa
            // 
            this.LabelKasa.AutoSize = true;
            this.LabelKasa.Location = new System.Drawing.Point(1412, 77);
            this.LabelKasa.Name = "LabelKasa";
            this.LabelKasa.Size = new System.Drawing.Size(125, 13);
            this.LabelKasa.TabIndex = 8;
            this.LabelKasa.Text = "Kasa dla jednego Kuriera";
            // 
            // CashToDel
            // 
            this.CashToDel.Location = new System.Drawing.Point(1415, 93);
            this.CashToDel.Name = "CashToDel";
            this.CashToDel.Size = new System.Drawing.Size(161, 20);
            this.CashToDel.TabIndex = 9;
            this.CashToDel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CashToDel_KeyPress);
            // 
            // MaxLen
            // 
            this.MaxLen.Location = new System.Drawing.Point(1415, 132);
            this.MaxLen.Name = "MaxLen";
            this.MaxLen.Size = new System.Drawing.Size(161, 20);
            this.MaxLen.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1413, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Długość Jednej trasy";
            // 
            // Slove
            // 
            this.Slove.Location = new System.Drawing.Point(1415, 720);
            this.Slove.Name = "Slove";
            this.Slove.Size = new System.Drawing.Size(160, 41);
            this.Slove.TabIndex = 12;
            this.Slove.Text = "Wyznacz trasy";
            this.Slove.UseVisualStyleBackColor = true;
            this.Slove.Click += new System.EventHandler(this.button1_Click);
            // 
            // RouteList
            // 
            this.RouteList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Number,
            this.len,
            this.cashh});
            this.RouteList.FullRowSelect = true;
            this.RouteList.GridLines = true;
            this.RouteList.Location = new System.Drawing.Point(12, 12);
            this.RouteList.MultiSelect = false;
            this.RouteList.Name = "RouteList";
            this.RouteList.Size = new System.Drawing.Size(229, 749);
            this.RouteList.TabIndex = 13;
            this.RouteList.UseCompatibleStateImageBehavior = false;
            this.RouteList.View = System.Windows.Forms.View.Details;
            this.RouteList.SelectedIndexChanged += new System.EventHandler(this.RouteList_SelectedIndexChanged);
            this.RouteList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.RouteList_MouseDoubleClick);
            // 
            // Number
            // 
            this.Number.Text = "Numer Trasy";
            // 
            // len
            // 
            this.len.Text = "Długość";
            this.len.Width = 70;
            // 
            // cashh
            // 
            this.cashh.Text = "Pieniądze ";
            this.cashh.Width = 102;
            // 
            // ListOfPoints
            // 
            this.ListOfPoints.Location = new System.Drawing.Point(12, 767);
            this.ListOfPoints.Name = "ListOfPoints";
            this.ListOfPoints.Size = new System.Drawing.Size(229, 41);
            this.ListOfPoints.TabIndex = 14;
            this.ListOfPoints.Text = "Lista punktów";
            this.ListOfPoints.UseVisualStyleBackColor = true;
            this.ListOfPoints.Click += new System.EventHandler(this.ListOfPoints_Click);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(1412, 767);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(163, 40);
            this.Close.TabIndex = 15;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1412, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "yellow for cash < 0 && cash <= 200";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1413, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "red for cash > 200 && cash <= 400";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1412, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "green for cash> 400 && cash<=600";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1413, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "blue for cash>600 && cash <1000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1416, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "purble for cash > 1000";
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1582, 820);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.ListOfPoints);
            this.Controls.Add(this.RouteList);
            this.Controls.Add(this.Slove);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MaxLen);
            this.Controls.Add(this.CashToDel);
            this.Controls.Add(this.LabelKasa);
            this.Controls.Add(this.TextBoxY);
            this.Controls.Add(this.TextBoxX);
            this.Controls.Add(this.YLabel);
            this.Controls.Add(this.Xlabel);
            this.Controls.Add(this.Wspolrzedne);
            this.Controls.Add(this.gMap);
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1600, 900);
            this.MinimumSize = new System.Drawing.Size(1598, 858);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl gMap;
        private System.Windows.Forms.Label Wspolrzedne;
        private System.Windows.Forms.Label Xlabel;
        private System.Windows.Forms.Label YLabel;
        private System.Windows.Forms.TextBox TextBoxX;
        private System.Windows.Forms.TextBox TextBoxY;
        private System.Windows.Forms.Label LabelKasa;
        private System.Windows.Forms.TextBox CashToDel;
        private System.Windows.Forms.TextBox MaxLen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Slove;
        private System.Windows.Forms.ListView RouteList;
        private System.Windows.Forms.ColumnHeader Number;
        private System.Windows.Forms.ColumnHeader len;
        private System.Windows.Forms.ColumnHeader cashh;
        private System.Windows.Forms.Button ListOfPoints;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

