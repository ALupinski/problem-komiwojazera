﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mapa
{
    class FileReader
    {

        public static int atmNumber, lenghtLimit;
        public static double cashLimit;
        public static List<atmData> atmList = new List<atmData>();
        public static List<atmData> h = new List<atmData>();
        public static atmData startPoint = new atmData();
        public static double toDouble(string a)
        {
            int index = a.IndexOf("e", 0);
            if (index > 0)
            {
                StringBuilder helper = new StringBuilder(a);
                helper[index] = ' ';
                helper[index + 1] = ' ';
                a = helper.ToString();
                string[] afterSplit = a.Split(' ');
                string dot = afterSplit[0];
                dot = dot.Replace('.', ',');
                double retDouble = double.Parse(dot) * (int)Math.Pow(10, int.Parse(afterSplit[2]));
                return retDouble;
            }
            else
                return double.Parse(a);
        }
        public static void GetListFromFile()
        {
            StreamReader file = new StreamReader("A.txt");
            string reader = file.ReadLine();
            string[] splitString = reader.Split(' ');
            atmNumber = int.Parse(splitString[0]);
            lenghtLimit = int.Parse(splitString[1]);
            cashLimit = int.Parse(splitString[2]);
            for (int i = 0; i < atmNumber; i++)
            {
                reader = file.ReadLine();
                splitString = reader.Split(' ');
                atmData newAdder = new atmData();
                newAdder.index = atmList.Count + 1;
                newAdder.x = toDouble(splitString[0]);
                newAdder.y = toDouble(splitString[1]);
                newAdder.cash = toDouble(splitString[2]);
                atmList.Add(newAdder);
                h.Add(newAdder);
            }
            reader = file.ReadLine();
            splitString = reader.Split(' ');
            startPoint.index = 0;
            startPoint.x = toDouble(splitString[0]);
            startPoint.y = toDouble(splitString[1]);

        }
        public static List<atmData> aboveStartPoint(List<atmData> atmListHelp)
        {
            List<atmData> above = new List<atmData>();

            foreach (atmData a in atmListHelp)
            {
                if (a.y >= startPoint.y)
                {
                    atmData newHelperPoint = new atmData(a);
                    above.Add(newHelperPoint);
                }

            }
            above.Sort((a, b) =>
            {
                return (int)(a.x - b.x);
            });
            return above;
        }
        public static List<atmData> underStartPoint(List<atmData> atmListHelp)
        {
            List<atmData> under = new List<atmData>();
            foreach (atmData a in atmListHelp)
            {

                if (a.y < startPoint.y)
                {

                    atmData newHelperPoint = new atmData(a);
                    under.Add(newHelperPoint);
                }
            }
            under.Sort((a, b) =>
            {
                return (int)(a.x - b.x);
            });
            under.Reverse();
            return under;
        }

    }
}
