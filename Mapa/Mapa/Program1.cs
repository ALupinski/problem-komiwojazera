﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapa
{
    class Program1
    {
        public static void StartAlg(string[] args)
        {
            DateTime startTime = DateTime.Now;
            FileReader.GetListFromFile();
            DistanceMatrix.createDistanceMatrix(FileReader.atmList);
            SweepAlgorithm.createListToSweep(FileReader.atmList);
            SweepAlgorithm.sweepAlgoithm();
            DateTime stopTime = DateTime.Now;
            TimeSpan timeForProgram = stopTime - startTime;
            Console.WriteLine("Czas pracy:" + timeForProgram.TotalMinutes);
            Console.ReadKey();
        }
    }
}
