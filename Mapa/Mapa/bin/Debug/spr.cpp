#include <stdio.h>
#include <math.h>
#include <string.h>
#include <vector>

using namespace std;

struct Punkt{
    float x, y;
    int p;
};
Punkt pts[500];
int dist[500][500];
int s, e, n;
int nn;
char nrProj;
char nazwa_pl[30];
int pom1, pom2, pom3, pom4, pom5;
void wczytaj_siecACDF(){
    FILE* plik=fopen(nazwa_pl, "r");

    fscanf(plik, "%d%d%d", &n, &pom1, &pom2);
    s=e=n+1;
    nn=n;
    int it=1;
    if(nrProj=='D'){
        fscanf(plik, "%d%d%d", &pom3, &pom4, &pom5);
        s=pom4+n;
        e=pom5+n;
        it=pom1;
    }
    for(int i=1;i<=n;i++)
        fscanf(plik, "%f%f%d", &(pts[i].x), &(pts[i].y), &(pts[i].p));
    while(it--){
        n++;
        fscanf(plik, "%f%f", &(pts[n].x), &(pts[n].y));
        pts[n].p=0;
    }
    for(int i=1;i<n;i++){
        for(int j=i+1;j<=n;j++){
            int d;
            if(nrProj=='A')
                d=(int)round(sqrt(pow((pts[i].x-pts[j].x), 2)+pow((pts[i].y-pts[j].y), 2)));
            else
                d=(int)floor(sqrt(pow((pts[i].x-pts[j].x), 2)+pow((pts[i].y-pts[j].y), 2)));
            dist[i][j]=dist[j][i]=d;
        }
    }
}
void wczytaj_siecB(){
    FILE* plik=fopen(nazwa_pl, "r");
    int x, y;
    fscanf(plik, "%d%d", &x, &y);
    n=2*x;
    for(int i=1;i<=n;i++){
        fscanf(plik, "%f%f", &(pts[i].x), &(pts[i].y));
        pts[i].p=0;
    }
    s=n+1;
    e=n+1;
    for(int i=1;i<=y;i++){
        n++;
        fscanf(plik, "%f%f", &(pts[n].x), &(pts[n].y));
        pts[n].p=0;
    }
    for(int i=1;i<n;i++){
        for(int j=i+1;j<=n;j++){
            int d=(int)round(sqrt(pow((pts[i].x-pts[j].x), 2)+pow((pts[i].y-pts[j].y), 2)));
            dist[i][j]=dist[j][i]=d;
        }
    }
}
vector<pair<int, int> > rozklady[101][101];
int t0, offset;
void wczytaj_siecE(){
    FILE* plik=fopen(nazwa_pl, "r");
    int m;
    fscanf(plik, "%d%d%d%d%d", &n, &m, &s, &t0, &offset);
    e=s;
    for(int i=1;i<=m;i++){
        int od, doo, k;
        fscanf(plik, "%d%d%d", &od, &doo, &k);
        for(int z=0;z<k;z++){
            int tt0, tt;
            fscanf(plik, "%d%d", &tt0, &tt);
            rozklady[od][doo].push_back(make_pair(tt0, tt));
        }
    }
}
vector<int>* odczytaj_trase(char* linia){
    vector<int>* trasa=new vector<int>();
    int len=strlen(linia);
    bool liczba=false;
    int aktW=0;
    for(int i=0;i<len;i++){
        char znak=linia[i];
        if(znak<='9' && znak>='0'){
            liczba=true;
            aktW*=10;
            aktW+=(znak-'0');
        }
        else{
            if(liczba){
                trasa->push_back(aktW);
                aktW=0;
                liczba=false;
            }
        }
    }
    return trasa;
}
bool sprawdz_trasyACDF(){
    FILE* plik=fopen("trasy.txt", "r");
    char linia[10000];
    fgets(linia, 100, plik);
    int ile=odczytaj_trase(linia)->at(0);
    int dlugoscG=0, profitG=0;
    bool odwiedzone[500];
    int ileOdw=1;
    memset(odwiedzone, 0, 500*sizeof(bool));

    for(int z=1;z<=ile;z++){
        vector<int> etapy;
        int dlugosc=0, profit=0;
        fgets(linia, 9900, plik);
        vector<int>* tr=odczytaj_trase(linia);
        int siz=tr->size();
        if(tr->at(0)!=s || tr->at(siz-1)!=e){
            printf("zły początek/koniec trasy\n");
            return false;
        }
        odwiedzone[s]=true;
        int pop=s;
        int dlAkt=0;
        for(int i=1;i<siz;i++){
            int akt=tr->at(i);
            dlugosc+=dist[pop][akt];
            dlAkt+=dist[pop][akt];
            if(nrProj=='D' && akt>nn){
                etapy.push_back(dlAkt);
                dlAkt=0;
            }
            if(i==siz-1)
                break;
            if(!odwiedzone[akt]){
                odwiedzone[akt]=true;
                profit+=pts[akt].p;
                ileOdw++;
            }
            else{
                if(nrProj!='D' || akt<=nn){
                    printf("duplikat %d\n", akt);
                    return false;
                }
            }
            pop=akt;
        }
        printf("trasa nr %d na długość %d i profit/gotowka %d\n", z, dlugosc, profit);
        if(nrProj=='A'){
            if(dlugosc>pom1){
                printf("za dluga trasa\n");
                return false;
            }
            if(profit>pom2){
                printf("za duzo gotowki w wozie\n");
                return false;
            }
        }
        else if(nrProj=='D'){
            printf("jest %d etapow o dlugosci: ", (int)etapy.size());
            if(etapy.size()!=pom2+1){
                printf("zla liczba etapow\n");
                return false;
            }
            for(int i=0;i<etapy.size();i++){
                printf("%d ", etapy[i]);
                if(etapy[i]>pom3){
                    printf("za dlugi etap miedzy ladowaniami\n");
                    return false;
                }
            }
            printf("\n");
        }
        else if(nrProj=='F'){
            if(dlugosc>pom2){
                printf("za dluga trasa\n");
                return false;
            }
        }
        dlugoscG+=dlugosc;
        profitG+=profit;
    }
    printf("łączna długość tras to %d łączny profit/gotowka to %d łącznie odwiedzono %d wierzchołków\n", dlugoscG, profitG, ileOdw);
    if(nrProj=='A'){
        if(ileOdw<n){
            printf("nie odwiedzono wszystkich bankomatow\n");
            return false;
        }
    }
    else if(nrProj=='C'){
        if(profitG<pom2){
            printf("nie osiagnieto zalozonego zysku\n");
            return false;
        }
    }
    return true;
}
bool sprawdz_trasyB(){

    FILE* plik=fopen("trasy.txt", "r");
    char linia[10000];
    fgets(linia, 100, plik);
    int ile=odczytaj_trase(linia)->at(0);
    int dlugoscG=0, profitG=0;
    bool odwiedzone[500];
    bool przesyl[500];
    int ileOdw=1;
    int ileObsl=0;
    memset(odwiedzone, 0, 500*sizeof(bool));
    for(int z=1;z<=ile;z++){
        int dlugosc=0, profit=0;
        fgets(linia, 9900, plik);
        vector<int>* tr=odczytaj_trase(linia);
        int siz=tr->size();
        if(tr->at(0)<s || tr->at(siz-1)!=tr->at(0)){
            printf("zły początek/koniec trasy\n");
            return false;
        }
        memset(przesyl, 0, 500*sizeof(bool));
        odwiedzone[tr->at(0)]=true;
        int ileP=0;
        int pop=tr->at(0);
        for(int i=1;i<siz;i++){
            int akt=tr->at(i);
            dlugosc+=dist[pop][akt];
            if(i==siz-1)
                break;
            if(!odwiedzone[akt]){
                odwiedzone[akt]=true;
                profit+=pts[akt].p;
                ileOdw++;
            }
            else{
                printf("duplikat %d\n", akt);
                return false;
            }
            if(akt%2==0){
                if(przesyl[akt-1]==false){
                    printf("proba dostarczenia paczki do %d bez jej wziecia z %d\n", akt, akt-1);
                    return false;
                }
                przesyl[akt-1]=false;
                ileObsl++;
            }
            else
                przesyl[akt]=true;
            pop=akt;
        }
        for(int i=0;i<=n;i++){
            if(przesyl[i]==true){
                printf("wzieto paczke z %d, ale jej nie dostarczono do %d\n", i, i+1);
                return false;
            }
        }
        printf("trasa nr %d na długość %d i profit/gotowka %d\n", z, dlugosc, profit);
        dlugoscG+=dlugosc;
        profitG+=profit;
    }
    printf("łączna długość tras to %d łączny profit/gotowka to %d łącznie obsluzono %d par nadawca-odbiorca\n", dlugoscG, profitG, ileObsl);
    if(ileObsl<nn){
        printf("nie obsluzono wszystkich nadawcow i odbiorcow\n");
        return false;
    }

    return true;
}
bool sprawdz_trasyE(){

    FILE* plik=fopen("trasy.txt", "r");
    char linia[10000];
    fgets(linia, 100, plik);
    int ile=odczytaj_trase(linia)->at(0);
    bool odwiedzone[500];
    int ileOdw=1;
    memset(odwiedzone, 0, 500*sizeof(bool));
    int dlugosc=0, profit=0;
        fgets(linia, 9900, plik);
        vector<int>* tr=odczytaj_trase(linia);
        int siz=tr->size();
        if(tr->at(0)!=s || tr->at(siz-1)!=e){
            printf("zły początek/koniec trasy\n");
            return false;
        }
        odwiedzone[s]=true;
        int ileP=0;
        int pop=s;
        int tAkt=t0;
        for(int i=1;i<siz;i++){
            int akt=tr->at(i);
            vector< pair<int, int> >* v=&rozklady[pop][akt];
            int ktory=-1;
            if(tAkt>v->at(v->size()-1).first){
                dlugosc+=(1440-tAkt);
                tAkt=0;
                ktory=0;
            }
            else
                ktory=lower_bound(v->begin(), v->end(), make_pair(tAkt, 0))-v->begin();
            dlugosc+=(v->at(ktory).first-tAkt);
            dlugosc+=(v->at(ktory).second);

            if(i<siz-1)
                dlugosc+=offset;
            tAkt=(v->at(ktory).first+v->at(ktory).second+offset)%1440;
            //printf("wyjazd z punktu %d w chwili %d (czas trwania %d), dojazd do %d w chwili %d (laczny czas trasy %d)\n", pop, v->at(ktory).first, v->at(ktory).second, akt, (v->at(ktory).first+v->at(ktory).second)%1440, dlugosc);
            if(i==siz-1)
                break;
            if(!odwiedzone[akt]){
                odwiedzone[akt]=true;
                ileOdw++;
            }
            else{
                printf("duplikat %d\n", akt);
                return false;
            }
            pop=akt;
        }
        printf("trasa ma czas trwania %d\n", dlugosc);
    printf("odwiedzono %d miast \n", ileOdw);
    if(ileOdw<n){
        printf("nie odwiedzono wszystkich miast\n");
        return false;
        }
    return true;
}
void wczytaj_projekt(){
    FILE* plik=fopen("projekt.txt", "r");
    fscanf(plik, "%c", &nrProj);
    sprintf(nazwa_pl, "%c.txt", nrProj);
    printf("projekt %c\n", nrProj);
    fclose(plik);
}
int main(){
    wczytaj_projekt();
    if(nrProj=='B'){
        wczytaj_siecB();
        sprawdz_trasyB();
    }
    else if(nrProj=='E'){
        wczytaj_siecE();
        sprawdz_trasyE();
    }
    else{
        wczytaj_siecACDF();
        sprawdz_trasyACDF();
    }
    return 0;
}
