﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapa
{

    class atmData
    {
        public int index;
        public double x;
        public double y;
        public double cash;
        public bool visited;
        public atmData()
        {
            index = 0;
            cash = 0;
            x = 0;
            y = 0;
            visited = false;
        }
        public atmData(atmData a)
        {
            index = a.index;
            cash = a.cash;
            x = a.x;
            y = a.y;
            visited = a.visited;
        }
    }

}
