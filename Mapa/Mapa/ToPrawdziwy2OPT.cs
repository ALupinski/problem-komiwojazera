﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapa
{
    class ToPrawdziwy2OPT
    {
        public bool H_twoOptSearch()
        {
            int n = points.Count();

            double best = traveledDistance, current = int.MaxValue;

            Route newRoute = new Route();

            for (int i = 1; i < n - 2; i++)
            {
                for (int j = i + 1; j < n - 1; j++)
                {
                    newRoute.points.Clear();
                    newRoute.points.AddRange(points);
                    newRoute.points.Reverse(i, j - i + 1);

                    current = newRoute.calculateDistance();

                    if (current < best)
                    {
                        //Console.WriteLine("2-OPT: Improve made by {0}\nSwapping points {1} and {2}...", best - current, points[i].id, points[j].id);

                        points.Clear();
                        points.AddRange(newRoute.points);

                        traveledDistance = current;
                        best = newRoute.calculateDistance();
                        return true;
                    }
                }
            }
            return false;
        }

    }
}
