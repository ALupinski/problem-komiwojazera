﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mapa
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            loadPointListToListView();
        }
        
        private int ind;

        private void PointList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PointList.SelectedItems.Count <= 0) return;
            else
            {
                ind = PointList.SelectedItems[0].Index;

               // int indexT = int.Parse(PointList.SelectedItems[ind].SubItems[0].Text);
                TextBoxX.Text = FileReader.atmList[ind].x.ToString();
                TextBoxY.Text = FileReader.atmList[ind].y.ToString();
                CashText.Text = FileReader.atmList[ind].cash.ToString();
            
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(FileReader.startPoint.index.ToString());
            this.Close();
            
        }

        private void Save_Click(object sender, EventArgs e)
        {
           

            //loadPointListToListView();

            if (ind <= 0) return;

            else
            {
                if (TextBoxX.Text != FileReader.atmList[ind].x.ToString() || TextBoxY.Text != FileReader.atmList[ind].y.ToString() || CashText.Text != FileReader.atmList[ind].cash.ToString())
                {
                    FileReader.atmList[ind].x = double.Parse(TextBoxX.Text);
                    FileReader.atmList[ind].y = double.Parse(TextBoxY.Text);
                    FileReader.atmList[ind].cash = double.Parse(CashText.Text);
                    PointList.Items.Clear();
                    // PointList.SelectedItems[ind].SubItems[1]=

                    loadPointListToListView();
                   
                }

            }
        }
        private void loadPointListToListView()
        {
            foreach (atmData a in FileReader.atmList)
            {
                ListViewItem newItem = new ListViewItem(a.index.ToString());
                newItem.SubItems.Add(a.x.ToString());
                newItem.SubItems.Add(a.y.ToString());
               // MessageBox.Show(a.cash.ToString());
                newItem.SubItems.Add(a.cash.ToString());
                PointList.Items.Add(newItem);
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            atmData newHelper = new atmData();
            newHelper.index = FileReader.atmList.Count;
            newHelper.x = double.Parse(TextBoxX.Text.Replace('.',','));
            newHelper.y = double.Parse(TextBoxY.Text.Replace('.', ','));
            newHelper.cash = double.Parse(CashText.Text.Replace('.', ','));
            FileReader.atmList.Add(newHelper);
            ListViewItem newItem = new ListViewItem(newHelper.index.ToString());
            newItem.SubItems.Add(newHelper.x.ToString());
            newItem.SubItems.Add(newHelper.y.ToString());
            newItem.SubItems.Add(newHelper.cash.ToString());
            PointList.Items.Add(newItem);
        }

        private void TextBoxX_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && TextBoxX.Text.IndexOf('.') != -1) 
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46) 
            {
                e.Handled = true;
            }
        }

        private void TextBoxY_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && TextBoxY.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void CashText_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && CashText.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void SetAsStartPoint_Click(object sender, EventArgs e)
        {
            if (TextBoxX.Text !=null && TextBoxY.Text != null)
            {
                FileReader.startPoint.x = double.Parse(TextBoxX.Text.Replace('.', ','));
                FileReader.startPoint.y = double.Parse(TextBoxY.Text.Replace('.', ','));
                FileReader.startPoint.index = ind;
            }
        }
    }
}
